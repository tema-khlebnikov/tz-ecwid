var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    csso = require('gulp-csso'),
    babel = require('gulp-babel'),
    // concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin');

var paths = {
  styles: ['_source/**/*.+(css|sass|scss)'],
  scripts: ['_source/**/*.js'],
  images: ['./_source/**/*.+(jpg|jpeg|png|gif|svg)']
};

gulp.task('styles', function(done) {
  gulp.src(paths.styles, { base: '_source' })
    .pipe(plumber())
    .pipe(sass()
      .on('error', function (err) {
        sass.logError(err);
        this.emit('end');
      })
    )
    .pipe(autoprefixer())
    .pipe(csso())
    .pipe(gulp.dest('./'));
  done();
});

gulp.task('scripts', function(done) {
  gulp.src(paths.scripts, { base: '_source' })
    .pipe(plumber())
//    .pipe(sourcemaps.init())
    .pipe(babel())
    // .pipe(concat("all.js"))
    .pipe(uglify())
//    .pipe(sourcemaps.write())
    .pipe(gulp.dest('.'));
  done();
});

gulp.task('images', function(done) {
  gulp.src(paths.images, { base: '_source' })
    .pipe(plumber())
    .pipe(imagemin())
    .pipe(gulp.dest('.'));
  done();
});

gulp.task('watch', function(done) {
  gulp.watch(paths.styles, gulp.series('styles'));
  gulp.watch(paths.scripts, gulp.series('scripts'));
  gulp.watch(paths.images, gulp.series('images'));
  done();
});

gulp.task('dev', gulp.series('watch', 'styles', 'scripts'));
gulp.task('build', gulp.series('styles', 'scripts', 'images'));
gulp.task('default', gulp.series('watch', 'styles', 'scripts', 'images'));