'use strict';



class MyGallery extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      galleryImages: [],
      maxHeight: 200,
      gutter: 10,
      loadUrl: 'https://don16obqbay2c.cloudfront.net/frontend-test-task/gallery-images.json'
    };

    // Начальная загрузка картинок
    let galleryImages = [];
    fetch('gallery-images.json')
      .then(response => response.json())
      .then(response => {
        galleryImages = response.galleryImages;
        galleryImages.map(image => {
          image.calcWidth = image.width;
          image.calcHeight = image.height;
        });
        this.setState({ galleryImages: galleryImages });
        this.recalc(this.state, this.props);
      });

    this.handleInputChange = this.handleInputChange.bind(this);

    // пересчёт галереи при изменении размера окна браузера
    window.addEventListener('resize', e => {
      this.recalc(this.state, this.props);
    }, false);

  }

  // обработка инпутов
  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : ( target.type === 'number' ? +target.value : target.value );
    const name = target.name;
    
    this.setState({
      [name]: value
    });

    if ( name == 'maxHeight' ) {
      this.recalc(this.state, this.props);
    }
  }

  render() {
    return (
      <div className="my-gallery">
        <div className="my-gallery-panel">
          <div className="my-gallery-panel__line">
            <ButtonClear this={this} />
            <ButtonLoad this={this} defaults={true} />
          </div>
          <div className="my-gallery-panel__line">
            <input name="loadUrl" type="text" size="80" value={this.state.loadUrl} onChange={this.handleInputChange} />
            <ButtonLoad this={this} defaults={false} />
          </div>
          <div className="my-gallery-panel__line">
            Максимальная высота строки <input name="maxHeight" type="range" value={this.state.maxHeight} onChange={this.handleInputChange} min="100" max="300" step="1" /> {this.state.maxHeight}
          </div>
        </div>
        <div className="my-gallery-container" style={{
          marginLeft: -this.state.gutter / 2,
          marginRight: -this.state.gutter / 2,
        }}>
          {this.state.galleryImages.map((image, index) => 
            <div key={index} className="my-gallery-image" style={{
              width: image.calcWidth + this.state.gutter,
              height: image.calcHeight,
              paddingLeft: this.state.gutter / 2,
              paddingRight: this.state.gutter / 2,
              marginBottom: this.state.gutter,
            }}>
              <img src={image.url} alt="" style={{
                width: image.calcWidth,
                height: image.calcHeight
              }} />
              <div className="my-gallery-image__cover">
                {image.width}x{image.height}<br />
                {Math.floor(image.calcWidth)}x{Math.floor(image.calcHeight)}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }


  // Пересчитывает размеры картинок, чтобы они вставали как надо
  recalc(state, props) {
    let galleryImages = state.galleryImages,
        maxHeight = state.maxHeight,
        gutter = state.gutter,
        containerWidth = document.querySelector(props.container).offsetWidth + gutter;

    let rowHeight,
        rowWidth = 0,
        rowFirstIndex = 0;

    galleryImages.map((image, index) => {
      let ar = image.width / image.height,
          calcWidth = maxHeight * ar;

      rowWidth += calcWidth + gutter;

      // если строка наполнена (или мы дошли до конца), вычисляем высоту строки
      if ( rowWidth >= containerWidth || index == galleryImages.length - 1 ) {
        // коэффициент, на который нужно уменьшить высоту
        // для его расчёта нужно вычесть нужное количество gutter
        let rowGuttersWidth = (index - rowFirstIndex + 1) * gutter,
            coeff = (containerWidth - rowGuttersWidth) / (rowWidth - rowGuttersWidth);

        coeff = Math.min(coeff, 1); // фикс для последней строки
        rowHeight = maxHeight * coeff;

        // обновляем всю строку
        for ( let i = rowFirstIndex; i <= index; i++ ) {
          let rowImage = galleryImages[ i ],
              ar = rowImage.width / rowImage.height;

          galleryImages[ i ].calcWidth = rowHeight * ar;
          // galleryImages[ i ].calcWidth = Math.floor(rowHeight * ar);
          galleryImages[ i ].calcHeight = rowHeight;
        }

        // сбрасываем некоторые переменные
        rowWidth = 0,
        rowFirstIndex = index + 1;
      } 
      
    });

    this.setState({ galleryImages: galleryImages });
  }

}

const e = React.createElement;
const domContainer = document.querySelector('#my-gallery-container');
ReactDOM.render(<MyGallery container="#my-gallery-container" />, domContainer);



function ButtonClear(props) {
  function handleClick(e) {
    e.preventDefault();
    props.this.setState({ galleryImages: [] });
  }

  return (
    <button onClick={handleClick}>
      Очистить список
    </button>
  );
}



// кнопка переиспользуется
function ButtonLoad(props) {
  function handleClick(e) {
    e.preventDefault();
    // используем замыкание
    let loadUrl = ( props.defaults ? 'gallery-images.json' : props.this.state.loadUrl );
    let galleryImages = props.this.state.galleryImages;
    fetch(loadUrl)
      .then(response => response.json())
      .then(response => {
        response.galleryImages.map(image => {
          image.calcWidth = image.width;
          image.calcHeight = image.height;
          galleryImages.push(image);
        });
        props.this.setState({ galleryImages: galleryImages });
        props.this.recalc(props.this.state, props.this.props);
      });
  }

  return (
    <button onClick={handleClick}>
      {props.defaults ? 'Добавить стандартный набор картинок' : 'Добавить набор картинок из URL'}
    </button>
  );
}
