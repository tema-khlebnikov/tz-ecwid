# tz-ecwid

## Демо

[http://tema-khlebnikov.ru/demo/tz-ecwid/](http://tema-khlebnikov.ru/demo/tz-ecwid/)

## Git репозиторий

[https://bitbucket.org/tema-khlebnikov/tz-ecwid/src/master/](https://bitbucket.org/tema-khlebnikov/tz-ecwid/src/master/) - проект уже собран, достаточно склонировать репозиторий и запустить `index.html`.

## Cобрать проект

Установка:

```npm install```

Сборка:

```gulp build```
